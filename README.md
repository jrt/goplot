# goplot

A low effort golang tool to play with [Plotterflut](https://team-tfm.com/plotterflut/).

## Features
- QR-Code plotting
- PNG plotting
- send pen home (0,0)
- Find a free rectangle of a given size
- Spline Logo plotting