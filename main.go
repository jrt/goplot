package main

import (
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"rsc.io/qr"
)

/*
To see it in action, you have to join the jitsi meeting where I have set up a camera pointing to the plotter.
https://jitsi.linux.it/PlotterFlut

The drawing area is divided into 111x86 squares, you can fill each square with a given intensity between 0 and 7, using the following URL:
https://team-tfm.com/plotterflut/api

Send your data as a GET-request with the following arguments: "x", "y", "intensity".
If your request takes a while to get an answer the plotter is most probably executing your command, responding with status 200 after it has finished.
If something went wrong you should get an answer immediately.
If the plotter is currently busy with another command, the response status code is 409.
If something went wrong (arguments out of bounds, field already drawn, parse errors), it answers with status code 402. Refer to the reponse text for details.

*/

const (
	url     = "https://team-tfm.com/plotterflut/api"
	dataurl = "https://team-tfm.com/plotterflut/data"

	canvasWidth  = 111
	canvasHeight = 86
)

//Pixel defines a Pixel on a canvas
type Pixel struct {
	X int `json:"x,string"`
	Y int `json:"y,string"`
	I int `json:"i,string"`
}

//Pos defines a position in a two dimensional space
type Pos struct {
	X int
	Y int
}

type PixMap map[Pos]int

func pixMapFromPixSlice(data []Pixel) PixMap {
	m := make(PixMap, len(data))
	for _, p := range data {
		m[Pos{p.X, p.Y}] = p.I
	}
	return m
}

func main() {
	//PlotRequest(100, 20, 5)
	//PlotSplineLogo()
	//PlotQrCode("https://bit.ly/3eeGVWc", 77, 50)

	/*
		img, err := loadPnG("/home/jrt/Downloads/android.png")
		if err != nil {
			log.Println("failed to load image")
			return
		}
		plotImg(img, 35, 0)
	*/
	//fmt.Println(FindFreeRec(26, 32))

	Home()
	//PlotFake3D(50, 50)
	//HorizontalLine(78, 5, 3)
	//VertLine(39, 65, 3)
}

func loadPnG(path string) (image.Image, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return png.Decode(file)
}

func plotImg(img image.Image, posX, posY int) {
	for i := img.Bounds().Min.Y; i < img.Bounds().Max.Y; i++ {
		for j := img.Bounds().Min.X; j < img.Bounds().Max.X; j++ {
			color := color.GrayModel.Convert(img.At(j, i)).(color.Gray)
			intensity := int(color.Y / 34) //51 for 0-5, 34 for 0-7
			if intensity == 0 {
				intensity = 23
			}
			PlotRequest(j+posX, i+posY, intensity)
		}
	}
}

func requestData() ([]Pixel, error) {
	resp, err := http.Get(dataurl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var data []Pixel
	if err = json.Unmarshal(body, &data); err != nil {
		return nil, err
	}

	return data, nil
}

// FindFreeRec finds a free rectangle of a given size (x,y)
func FindFreeRec(sizeX, sizeY int) []Pos {
	data, err := requestData()
	if err != nil {
		log.Printf("Could not request data: %s\n", err)
		return nil
	}

	m := pixMapFromPixSlice(data)
	s := Pos{sizeX, sizeY}
	var p []Pos

	for y := 0; y < canvasHeight-sizeY; y++ {
		for x := 0; x < canvasWidth-sizeX; x++ {
			if checkFree(m, Pos{x, y}, s) {
				p = append(p, Pos{x, y})
			}
		}
	}

	return p
}

func checkFree(m PixMap, start Pos, size Pos) bool {
	for y := 0; y < size.Y; y++ {
		for x := 0; x < size.X; x++ {
			if _, ok := m[Pos{start.X + x, start.Y + y}]; ok {
				return false
			}
		}
	}

	return true
}

//PlotQrCode plots a QR code from string
func PlotQrCode(text string, offsetX, offsetY int) {
	code, err := qr.Encode(text, qr.L)
	if err != nil {
		return
	}

	if offsetX+code.Size+1 >= canvasWidth || offsetY+code.Size+1 >= canvasHeight {
		log.Println("Out of bounds")
		return
	}

	for i := 0; i <= code.Size; i++ {
		for j := 0; j <= code.Size; j++ {
			if code.Black(j, i) {
				PlotRequest(offsetX+j, offsetY+i, 5)
			} else {
				PlotRequest(offsetX+j, offsetY+i, 23)
			}
		}
	}
}

// PlotSplineLogo uses PlotRequest to Plot the Spline Logo
func PlotSplineLogo(offsetX int, offsetY int) {

	// Line 0
	PlotRequest(offsetX+2, offsetY+0, 3)
	PlotRequest(offsetX+8, offsetY+0, 3)

	// Line 1
	PlotRequest(offsetX+1, offsetY+1, 3)
	PlotRequest(offsetX+2, offsetY+1, 3)
	PlotRequest(offsetX+3, offsetY+1, 3)
	PlotRequest(offsetX+7, offsetY+1, 3)
	PlotRequest(offsetX+8, offsetY+1, 3)
	PlotRequest(offsetX+9, offsetY+1, 3)

	// Line 2
	PlotRequest(offsetX+1, offsetY+2, 3)
	PlotRequest(offsetX+2, offsetY+2, 3)
	PlotRequest(offsetX+3, offsetY+2, 3)
	PlotRequest(offsetX+7, offsetY+2, 3)
	PlotRequest(offsetX+8, offsetY+2, 3)
	PlotRequest(offsetX+9, offsetY+2, 3)

	//Line 3
	PlotRequest(offsetX+2, offsetY+3, 3)
	PlotRequest(offsetX+8, offsetY+3, 3)

	// Line 4
	var maxX int = 10
	for maxX >= 0 {
		PlotRequest(offsetX+maxX, offsetY+4, 3)
		maxX--
	}
	//Line 5
	PlotRequest(offsetX+5, offsetY+5, 3)

	// Line 6
	PlotRequest(offsetX+4, offsetY+6, 3)
	PlotRequest(offsetX+5, offsetY+6, 3)
	PlotRequest(offsetX+6, offsetY+6, 3)

	// Line 7
	PlotRequest(offsetX+4, offsetY+7, 3)
	PlotRequest(offsetX+5, offsetY+7, 3)
	PlotRequest(offsetX+6, offsetY+7, 3)

	// Line 8
	PlotRequest(offsetX+5, offsetY+8, 3)
}

// PlotRequest sends a request to the Plotter to plot a pixel at (x|y) with a given intensity between 0-7
func PlotRequest(x int, y int, intensity int) {
	if (intensity >= 8 || intensity < 0) && intensity != 23 {
		log.Fatal("intensity is not in the range of 0-5 or 23(block)")

	}
	if x >= canvasWidth || x < 0 {
		log.Fatal("x is not in rang")
	}
	if y >= canvasHeight || y < 0 {
		log.Fatal("y is not in range")
	}
	requrl := fmt.Sprintf("%s?x=%d&y=%d&intensity=%d", url, x, y, intensity)
	resp, err := http.Get(requrl)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	//body, err := ioutil.ReadAll(resp.Body)
	//log.Printf(string(body))
	if err != nil {
		log.Fatalln(err)
	}

	// 409 Plotter busy, retry
	if resp.StatusCode == 409 {
		log.Println("409 - Plotter busy, retry")
		PlotRequest(x, y, intensity)
	} else if resp.StatusCode == 503 {
		log.Println("503 - Rate Limit 5 req/s exceeded. wait")
		time.Sleep(200)
		PlotRequest(x, y, intensity)
		log.Println("retry")
	} else if resp.StatusCode == 200 {
		log.Println("200 - Success")
	}

}

// PlotFake3D plots a semi 3D sculpture
func PlotFake3D(offsetX int, offsetY int) {

	HorizontalLine(offsetX+1, offsetY+0, 10, 5)
	HorizontalLine(offsetX+0, offsetY+2, 10, 5)
	HorizontalLine(offsetX+0, offsetY+7, 10, 5)

	VertLine(offsetX+0, offsetY+1, 7, 5)
	VertLine(offsetX+8, offsetY+2, 6, 5)
	VertLine(offsetX+10, offsetY+0, 7, 5)
	VertLine(offsetX+2, offsetY+1, 5, 1)
	PlotRequest(offsetX+1, offsetY+6, 1)
	HorizontalLine(offsetX+2, offsetY+5, 7, 1)

}

//HorizontalLine plots a Line
func HorizontalLine(offsetX int, offsetY int, len int, intensity int) {
	for len >= 0 {
		PlotRequest(offsetX+len, offsetY, intensity)
		len--
	}

}

//VertLine plots a Line
func VertLine(offsetX int, offsetY int, height int, intensity int) {
	for height >= 1 {
		PlotRequest(offsetX, offsetY+height, intensity)
		height--
	}

}

// Home sends the head of the plotter in the top left corner (0|0)
func Home() {
	resp, err := http.Get("https://team-tfm.com/plotterflut/home")
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	if err != nil {
		log.Fatalln(err)
	}

	// StatusCode handling
	if resp.StatusCode == 409 {
		log.Println("409 - Plotter busy, retry")
		Home()
	} else if resp.StatusCode == 503 {
		log.Println("503 - Rate Limit 5 req/s exceeded. wait")
		time.Sleep(200)
		Home()
		log.Println("retry")
	} else if resp.StatusCode == 200 {
		log.Println("200 - Head at 0,0")
	}

}
